package model.util;

import model.vo.VOMovingViolation;
/******************************************************************************
 *  Compilation:  javac Shell.java
 *  Execution:    java Shell < input.txt
 *  Dependencies: StdOut.java StdIn.java
 *  Data files:   https://algs4.cs.princeton.edu/21elementary/tiny.txt
 *                https://algs4.cs.princeton.edu/21elementary/words3.txt
 *   
 *  Sorts a sequence of strings from standard input using shellsort.
 *
 *  Uses increment sequence proposed by Sedgewick and Incerpi.
 *  The nth element of the sequence is the smallest integer >= 2.5^n
 *  that is relatively prime to all previous terms in the sequence.
 *  For example, incs[4] is 41 because 2.5^4 = 39.0625 and 41 is
 *  the next integer that is relatively prime to 3, 7, and 16.
 *   
 *  % more tiny.txt
 *  S O R T E X A M P L E
 *
 *  % java Shell < tiny.txt
 *  A E E L M O P R S T X                 [ one string per line ]
 *    
 *  % more words3.txt
 *  bed bug dad yes zoo ... all bad yet
 *  
 *  % java Shell < words3.txt
 *  all bad bed bug dad ... yes yet zoo    [ one string per line ]
 *
 *
 ******************************************************************************/

public class Sort 
{
	/**
	 *  The {@code Shell} class provides static methods for sorting an
	 *  array using Shellsort with Knuth's increment sequence (1, 4, 13, 40, ...).
	 *  <p>
	 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/21elementary">Section 2.1</a> of
	 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
	 *  
	 *  @author Robert Sedgewick
	 *  @author Kevin Wayne
	 */

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public static void ordenarShellSort(Comparable[] datos)
	{

		int n = datos.length;
		int h = 1;
		while (h < n / 3)
			h = 3 * h + 1;

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(datos[j], datos[j - h]); j -= h) {
					exchange(datos, j, j - h);
				}
			}

			h /= 3;
		}

	}
	
	private static Comparable[] aux;
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public static void ordenarMergeSort(Comparable[] datos)
	{
		aux = new Comparable[datos.length];
		sort(datos, 0, datos.length-1);
		
//		int n = datos.length;
//		Comparable[] aux = new Comparable[n];
//		sortParaMerge(datos, aux, 0, n);
	}
	
	private static void sort(Comparable[] datos, int lo, int hi)
	{
		if(hi <= lo) return;
		int mid = lo + (hi - lo)/2;
		sort(datos, lo, mid);
		sort(datos, mid+1, hi);
		merge(datos, lo, mid, hi);
	}
	
	private static void merge(Comparable[] datos, int lo, int mid, int hi)
	{
		int i = lo;
		int j = mid+1;
		for(int k = lo; k<= hi; k++)
			aux[k] = datos[k];
		for(int k = lo; k <= hi; k++)
			if( i > mid) datos[k] = aux[j++];
			else if(j > hi) datos[k] = aux[i++];
			else if(less(aux[j], aux[i])) datos[k] = aux[j++];
			else datos[k] = aux[i++];
	}


	public static void sortParaMerge(Comparable[] a, Comparable[] aux, int lo, int hi) {

		// base case
		if (hi - lo <= 1) return;
		int contador =0;
		int mid = lo + (hi - lo) / 2;
		// sort each half, recursively
		if (true){
			contador++;

			sortParaMerge(a, aux, lo, mid);
			sortParaMerge(a, aux, mid, hi);
		}
		// merge back together
		if (contador==2){
			int i = lo, j = mid;
			for (int k = lo; k < hi; k++) {
				if      (i == mid)                 aux[k] = a[j++];
				else if (j == hi)                  aux[k] = a[i++];
				else if (a[j].compareTo(a[i]) < 0) aux[k] = a[j++];
				else                               aux[k] = a[i++];
			}

			// copy back
			for (int k = lo; k < hi; k++)
				a[k] = aux[k];
		}
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public static void ordenarQuickSort(Comparable[] datos, int lo, int hi) 
	{
		if(hi <= lo) return;
		
		int j = partition(datos, lo, hi);
		
		ordenarQuickSort(datos, lo, j-1);
		ordenarQuickSort(datos, j+1, hi);
	}
	
	private static int partition(Comparable[] datos, int lo, int hi)
	{
		int i = lo;
		int j = hi+1;
		Comparable aux  = datos[lo];
		while(true)
		{
			while(less(datos[++i], aux)) if(i == hi) break;
			while(less(aux, datos[--j])) if(j == lo) break;
			if(i >= j) break;
			exchange(datos, i, j);
		}
		exchange(datos, lo, j);
		return j;
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * 
	 * @param v
	 *            primer objeto de comparacion
	 * @param w
	 *            segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en
	 *         caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w) {

		return v.compareTo(w) < 0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * 
	 * @param datos
	 *            contenedor de datos
	 * @param i
	 *            posicion del 1er elemento a intercambiar
	 * @param j
	 *            posicion del 2o elemento a intercambiar
	 */
	private static void exchange(Comparable[] datos, int i, int j) {

		Comparable actual = datos[i];
		datos[i] = datos[j];
		datos[j] = actual;
	}
	

/******************************************************************************
 *  Copyright 2002-2018, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/

	

}
