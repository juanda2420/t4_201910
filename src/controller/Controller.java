package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import model.util.Sort;
import model.vo.VOMovingViolation;
import view.MovingViolationsManagerView;

@SuppressWarnings("unused")
public class Controller {

	private MovingViolationsManagerView view;

	private LinkedList<VOMovingViolation> listaStack;

	// Muestra obtenida de los datos cargados
	Comparable<VOMovingViolation>[] muestra;

	// Copia de la muestra de datos a ordenar
	Comparable<VOMovingViolation>[] muestraCopia;

	public Controller() {
		view = new MovingViolationsManagerView();
		listaStack = new LinkedList<VOMovingViolation>();
	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe
	 * ser Comparable para ser usada en los ordenamientos. Todas infracciones
	 * (MovingViolation) deben almacenarse en una Estructura de Datos (en el
	 * mismo orden como estan los archivos) A partir de estos datos se obtendran
	 * muestras para evaluar los algoritmos de ordenamiento
	 * 
	 * @return numero de infracciones leidas
	 */
	public int loadMovingViolations(String archivoJanuary, String archivoFebruary, String archivoMarch) {
		int x, y, z;
		x = loadJanuary(archivoJanuary);
		y = loadFebruary(archivoFebruary);
		z = loadMarch(archivoMarch);

		return x + y + z;
	}

	public int loadJanuary(String archivoJanuary) {
		int contador = 0;
		File archivo = new File(archivoJanuary);
		try {
			System.out.println(archivoJanuary);
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
					
				}

				VOMovingViolation nuevo = new VOMovingViolation(datosActual.get(0), datosActual.get(1),
						datosActual.get(2), datosActual.get(3), datosActual.get(4), datosActual.get(5),
						datosActual.get(6), datosActual.get(7), datosActual.get(8), datosActual.get(9),
						datosActual.get(10), datosActual.get(11), datosActual.get(12), datosActual.get(13),
						datosActual.get(14), datosActual.get(15));

				listaStack.add(nuevo);
				contador++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return contador;
	}

	public int loadFebruary(String archivoJanuary) {
		int contador = 0;
		File archivo = new File(archivoJanuary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
					//contador++;
				}

				VOMovingViolation nuevo = new VOMovingViolation(datosActual.get(0), datosActual.get(1),
						datosActual.get(2), datosActual.get(3), datosActual.get(4), datosActual.get(5),
						datosActual.get(6), datosActual.get(7), datosActual.get(8), datosActual.get(9),
						datosActual.get(10), datosActual.get(11), datosActual.get(12), datosActual.get(13),
						datosActual.get(14), datosActual.get(15));

				listaStack.add(nuevo);
				contador++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return contador;
	}

	public int loadMarch(String archivoJanuary) {
		int contador = 0;
		File archivo = new File(archivoJanuary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
					//contador++;
				}

				VOMovingViolation nuevo = new VOMovingViolation(datosActual.get(0), datosActual.get(1),
						datosActual.get(2), datosActual.get(3), datosActual.get(4), datosActual.get(5),
						datosActual.get(6), datosActual.get(7), datosActual.get(8), datosActual.get(9),
						datosActual.get(10), datosActual.get(11), datosActual.get(12), datosActual.get(13),
						datosActual.get(14), datosActual.get(15));

				listaStack.add(nuevo);
				contador++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return contador;
	}

	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos. Los datos
	 * de la muestra se obtienen de las infracciones guardadas en la Estructura
	 * de Datos.
	 * 
	 * @param n
	 *            tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	@SuppressWarnings("unchecked")
	public Comparable<VOMovingViolation>[] generarMuestra(int n) 
	{

		if ( n <= 0 || n > 240000 ) 
			return null;

		else 
		{
			muestra = new Comparable[n];

			int contador = 0;
			int aleatorio = 0;

			while (contador != n) 
			{
				aleatorio = (int) ((Math.random()) * n);
				muestra[contador] = listaStack.get(aleatorio);
				contador++;
			}

		}

		return muestra;
	}

	/**
	 * Generar una copia de una muestra. Se genera un nuevo arreglo con los
	 * mismos elementos.
	 * 
	 * @param muestra
	 *            - datos de la muestra original
	 * @return copia de la muestra
	 */
	public Comparable<VOMovingViolation>[] obtenerCopia(Comparable<VOMovingViolation>[] muestra) {
		Comparable<VOMovingViolation>[] copia = new Comparable[muestra.length];
		for (int i = 0; i < muestra.length; i++) {
			copia[i] = muestra[i];
		}
		return copia;
	}

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public void ordenarShellSort(Comparable<VOMovingViolation>[] datos) {

		Sort.ordenarShellSort(datos);
	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public void ordenarMergeSort(Comparable<VOMovingViolation>[] datos) {

		Sort.ordenarMergeSort(datos);
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * 
	 * @param datos
	 *            - conjunto de datos a ordenar (inicio) y conjunto de datos
	 *            ordenados (final)
	 */
	public void ordenarQuickSort(Comparable<VOMovingViolation>[] datos) 
	{
		Sort.ordenarQuickSort(datos, 0 , datos.length -1);
	}

	/**
	 * Invertir una muestra de datos (in place). datos[0] y datos[N-1] se
	 * intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y
	 * datos[N-3] se intercambian, ...
	 * 
	 * @param datos
	 *            - conjunto de datos a invertir (inicio) y conjunto de datos
	 *            invertidos (final)
	 */
	public void invertirMuestra(Comparable[] datos) 
	{

		int n = datos.length;
		int contador = 0;
		while (contador != n / 2) 
		{

			Comparable temporal = datos[contador];
			datos[n - contador - 1] = temporal;
			contador++;
		}
	}

	public void run() {
		long startTime;
		long endTime;
		long duration;

		int nDatos = 0;
		int nMuestra = 0;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 1:
				// Cargar infracciones
				nDatos = this.loadMovingViolations("data/Moving_Violations_Issued_in_January_2018.csv",
						"data/Moving_Violations_Issued_in_February_2018.csv",
						"data/Moving_Violations_Issued_in_March_2018.csv");
				view.printMensage("Numero infracciones cargadas:" + nDatos);
				break;

			case 2:
				// Generar muestra de infracciones a ordenar
				view.printMensage("Dar tamaNo de la muestra: ");
				nMuestra = sc.nextInt();
				muestra = this.generarMuestra(nMuestra);
				view.printMensage("Muestra generada");
				break;

			case 3:
				// Mostrar los datos de la muestra actual (original)
				if (nMuestra > 0 && muestra != null && muestra.length == nMuestra) {
					view.printDatosMuestra(nMuestra, muestra);
				} else {
					view.printMensage("Muestra invalida");
				}
				break;

			case 4:
				// Aplicar ShellSort a una copia de la muestra
				if (nMuestra > 0 && muestra != null && muestra.length == nMuestra) {
					muestraCopia = this.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					this.ordenarShellSort(muestraCopia);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMensage("Ordenamiento generado en una copia de la muestra");
					view.printMensage("Tiempo de ordenamiento ShellSort: " + duration + " milisegundos");
				} else {
					view.printMensage("Muestra invalida");
				}
				break;

			case 5:
				// Aplicar MergeSort a una copia de la muestra
				if (nMuestra > 0 && muestra != null && muestra.length == nMuestra) {
					muestraCopia = this.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					this.ordenarMergeSort(muestraCopia);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMensage("Ordenamiento generado en una copia de la muestra");
					view.printMensage("Tiempo de ordenamiento MergeSort: " + duration + " milisegundos");
				} else {
					view.printMensage("Muestra invalida");
				}
				break;

			case 6:
				// Aplicar QuickSort a una copia de la muestra
				if (nMuestra > 0 && muestra != null && muestra.length == nMuestra) {
					muestraCopia = this.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					this.ordenarQuickSort(muestraCopia);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMensage("Ordenamiento generado en una copia de la muestra");
					view.printMensage("Tiempo de ordenamiento QuickSort: " + duration + " milisegundos");
				} else {
					view.printMensage("Muestra invalida");
				}
				break;

			case 7:
				// Mostrar los datos de la muestra ordenada (muestra copia)
				if (nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra) {
					view.printDatosMuestra(nMuestra, muestraCopia);
				} else {
					view.printMensage("Muestra Ordenada invalida");
				}
				break;

			case 8:
				// Una muestra ordenada se convierte en la muestra a ordenar
				if (nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra) {
					muestra = muestraCopia;
					view.printMensage("La muestra ordenada (copia) es ahora la muestra de datos a ordenar");
				}
				break;

			case 9:
				// Invertir la muestra a ordenar
				if (nMuestra > 0 && muestra != null && muestra.length == nMuestra) {
					this.invertirMuestra(muestra);
					view.printMensage("La muestra de datos a ordenar fue invertida");
				} else {
					view.printMensage("Muestra invalida");
				}

				break;

			case 10:
				fin = true;
				sc.close();
				break;
			}
		}
	}

}
