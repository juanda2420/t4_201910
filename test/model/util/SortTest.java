package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable[] datos1 = {3, 5, 54, 124, 43, 23545, 53, 5243, 2545, 4 ,53, 24, 7675,
			                        324324, 234, 432, 32, 433, 6, 654, 875, 34, 64, 36, 422};
	private Comparable[] datos2 = {"hola", "chao" ,"la", "concha", "de", "la", "lora", "juan","palabras",
			"moco","mica","inteligencia", "bobo", "creatividad", "jardin", "john", "bogota", "rasgado",
			"tu", "curso"};
	
	private Comparable[] datos3 = null;


	@Test
	public void testMergeSort1() 
	{
		Sort.ordenarMergeSort(datos1);
		
		boolean isSorted = false;
		for(int i = 1; i < datos1.length ;i++)
			if(datos1[i].compareTo(datos1[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	@Test
	public void testMergeSort2() 
	{
		Sort.ordenarMergeSort(datos2);
		
		boolean isSorted;
		for(int i = 1; i < datos2.length;i++)
			if(datos2[i].compareTo(datos2[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	@Test
	public void testQuickSort1() 
	{
		Sort.ordenarMergeSort(datos1);
		
		boolean isSorted;
		for(int i = 1; i < datos1.length ;i++)
			if(datos1[i].compareTo(datos1[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	@Test
	public void testQuickSort2() 
	{
		Sort.ordenarMergeSort(datos2);
		
		boolean isSorted;
		for(int i = 1; i < datos2.length ;i++)
			if(datos2[i].compareTo(datos2[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	@Test
	public void testShellSort1() 
	{
		Sort.ordenarMergeSort(datos1);
		
		boolean isSorted;
		for(int i = 1; i < datos1.length;i++)
			if(datos1[i].compareTo(datos1[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}
	
	@Test
	public void testShellSort2() 
	{
		Sort.ordenarMergeSort(datos2);
		
		boolean isSorted;
		for(int i = 1; i < datos2.length ;i++)
			if(datos2[i].compareTo(datos2[i-1]) < 0) isSorted = false;
		isSorted = true;
		assertEquals("No se orden� correctamente",true, isSorted);
	}

}
